#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shlex
import pipe_c as pipe
import nuevo2 as sim
import shutil


pars = []

FWHM=0.92 #macon seeing
theta= 0.3 #dadas las longitudes de onda y el diametro de 0.6m
N=1024
b_line=22
phi_0=.2
t_decay=12
beta=0
zp=27.
fov=900
nstars=7
alpha = 45
N2= 512 # 256
sep='      '

for j in (0,1,2,3,4):
	for Dl in (10,30,60,120):
		for gx_size in (30,50):
			for gx_mabs in (-18.5,-19.5,-20.5):
				for rate in (1,2,4):
					for gx_n in (1.,2.,3.,4.):
						for r_t in (.1,.75,1.5):
							for SN in (3,5):
							pars.append([FWHM,N,N2,SN,gx_n,alpha,theta,gx_size,gx_mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,nstars])



def motherfunction(pars):
	parent_dir=os.path.abspath('../Imagenes')
	newdir='testN_'+str(Dl)+'_'+str(gx_size)+'_'+str(gx_mabs)+'_'+str(rate)+'_'+str(gx_n)+'_'+str(r_t)+'_'+str(SN)
	newdir=os.path.join(parent_dir,newdir)
	if not os.path.exists(newdir):
	#comm='mkdir '+newdir
	#subprocess.call(shlex.split(comm))
	os.mkdir(newdir)
	sim.gen1(newdir,FWHM,N,N2,SN,gx_n,alpha,theta,gx_size,gx_mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,nstars)
	with open('../Resultados/results.dat','a') as results:
		#results.write('\n')
		results.write(sep+str(Dl)+sep+str(gx_size)+sep+str(gx_mabs)+sep+str(rate)+sep+str(gx_n)+sep+str(r_t)+sep+str(SN)+sep)
	pipe.pipeline(newdir)
	shutil.rmtree(newdir)


