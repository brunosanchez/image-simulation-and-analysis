 #!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as np
from astropy.io.fits import getheader
import subprocess
import shlex

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------------------WARNINGS-----------------------------------#
#This program uses Source Extractor (from Emmanuel Bertin), this is software 
#from Astromatic.net, for further information visit
# https://www.astromatic.net/software/sextractor
# or send an e-mail to the author bertin@iap.fr
#----------------------------------------------------------------------------%
#this script uses path definitions were there should be the following objects:
        #Configuration files for SOURCE EXTRACTOR
        #The images to analize        
	#the future catalogs to be printed by this program    
#IT IS EXTREMELY IMPORTANT TO CHANGE THEM 
#-----------------------------------------------------------------------------#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def count_lines2(path):
    lines= sum(1 for line in open(path))
    return(lines)

from astropy.io import fits
from astropy.time import Time

def Diff_capsule_corp(p1,gal, t, t_exp, i,zero,FWHM,res):
        path=os.path.abspath(p1)
        s=np.shape(gal)
        for l in range(0,s[0]):
            for j in range(0, s[1]):
                gal[l,j]=round(gal[l,j])
       	file1=fits.PrimaryHDU(gal)
	hdulist=fits.HDUList([file1])
	hdr=hdulist[0].header
	time=Time(t, format='jd', scale='utc')
	dia = time.iso[0:10]
	hora = time.iso[11:24]
	jd= time.jd
	hdr.set('TIME-OBS', hora)
	hdr.set('DATE-OBS', dia)
	hdr.set('EXPTIME', t_exp)
	hdr.set('JD', jd)
	hdr.set('ZERO_P',zero)
	hdr.set('PX_SCALE',res)
	hdr.set('S_FWHM',FWHM)
	path_fits=os.path.join(path,('Diff_image'+str(i).zfill(5)+'.fits'))
	hdulist.writeto(path_fits)


def pipeline(path_test):
    #getting the absolute path to the images directory
    p=os.path.abspath(path_test)
    #moving to it
    os.chdir(p)
    f =open('test.cat','a+')
    f.close()
    #create a string naming the future image list
    l='listafits.dat'
    path1=os.path.abspath('image00001.fits')
    #use an unix command to fill the list with our images names
    os.system('ls image*.fits > '+l)
    #getting length of the list
    p1=os.path.abspath(l)
    number=count_lines2(p1)
    print(number)
    #open the list file
    listafits=open(os.path.join(p,l))
    #creating the SExtractor catalog names
    catalog=os.path.join(p,'test.cat')
    #naming the configuration file for SExtractor (SE)
    config=os.path.join('../../codigos','default.sex.ccd_example')
    #creating the command for calling SE and using it
    command='sex '+ path1+' -c '+config+' -CATALOG_NAME '+catalog#+' -PIXEL_SCALE '+str(px_s)+' SEEING_FWHM '+str(fw) 
    arg=shlex.split(command)
    subprocess.call(arg)
    #this is for detecting the sources in the first image
    #and to extract useful information for the rest of the images
    N_max_sources=count_lines2(catalog)	
    #checking the number of sources and creating the data-structures of the 
    #ligthcurves, with 17 for the number of columns of SE (16) and the time column
    base_data=np.zeros(shape=(number,N_max_sources,17))
    #starting the loop of the total of the images
    for i in range(number):
        #reading fits name
        name=listafits.readline()
        #print(name) #  --- for debugging
        path_fits=os.path.abspath(name)
        #print(path_fits)  #  ---- for debugging
        #getting image's header, for reading the date, and further details (in case of need)
        Hdr=getheader(path_fits)
        #for the moment just using the date
        tiempo=Hdr['JD']
        fw=round(float(Hdr['S_FWHM']),2)
        px_s=round(float(Hdr['PX_SCALE']),3)
        #execute sextractor
            #building commands
        command='sex '+ path1+','+path_fits+' -c '+config+' -CATALOG_NAME '+catalog#+' -PIXEL_SCALE '+str(px_s)+' SEEING_FWHM '+str(fw) 
        arg=shlex.split(command)
            #calling them
        subprocess.call(arg)
        #opening catalogs from SE
        cat=open(catalog, 'r')
        j=0
        #starting loop over the catalog lines
        for line in cat:
            if line:
                info = line.split()
                info.append(tiempo)
                base_data[i,j,:]=info
                j=j+1
	cat.close()
    #now printing on different files for each source
    #check the PATH TWICE!
    for i in range(N_max_sources):
        s1='source'+str(i+1)+'.dat'
        filename=os.path.join(p,s1)
        np.savetxt(filename,base_data[:,i,:])
    listafits.close()

#----------------------------------------------------------------
#----------------------------------------------------------------
#Starting the pipeline using Differential Image Photometry (DIA)
#----------------------------------------------------------------
#----------------------------------------------------------------

    f =open('test_d.cat','a+')
    f.close()
    catalog=os.path.join(p,'test_d.cat')
    listafits=open(os.path.join(p,l))
    #creating catalog names for SE
    config=os.path.join('../../codigos','default.sex.differential')
    Sci_M = fits.open(path1)[0].data
    base_data=np.zeros(shape=(1,17))
    listafits.readline()
    for i in range(number-1):  
        path_fits=listafits.readline()
        #print path_fits   # ---- for debugging
        path_diff_fits = os.path.join(p,('Diff_image'+str(i+2).zfill(5)+'.fits'))
        if not os.path.exists(path_diff_fits):
        #using this to avoid crush if the file already exists
            if not path_fits == 'image00001.fits':
                #avoiding to use the first image, just the next ones
                #reading important information from the original fits header
                hdulist   =  fits.open(path_fits)
                scidata_U =  hdulist[0].data
                diff_data =  scidata_U - Sci_M
                t         =  hdulist[0].header['JD']
                t_exp     =  hdulist[0].header['EXPTIME']
                zero      =  hdulist[0].header['ZERO_P']
                FWHM      =  hdulist[0].header['S_FWHM']
                res       =  hdulist[0].header['PX_SCALE']
                #encapsuling the difference of images in a new fits
                Diff_capsule_corp(p,diff_data,t,t_exp,i+2,zero,FWHM,res)
        Hdr=fits.getheader(path_fits)
        tiempo=Hdr['JD']
        #execute sextractor
            #building commands
        command='sex '+path_diff_fits+' -c '+config+' -CATALOG_NAME '+catalog#+' -PIXEL_SCALE '+str(px_s)+' SEEING_FWHM '+str(fw) 
        arg=shlex.split(command)
            #calling them
        subprocess.call(arg)
        cat=open(catalog, 'r')
        #starting loop over the catalog lines
        for line in cat:
            if line:
                info = line.split()
		#print info  # ---- for debugging
                info.append(tiempo)
                base_data = np.append(base_data, [info], axis=0)
	#print base_data  # ---- for debugging
        cat.close()
    #print base_data   # ---- for debugging
    np.savetxt(fname='Diff.dat', X=base_data.astype(np.float))
    rscript=shlex.split('Rscript ../../codigos/engine.R')
    subprocess.call(rscript)
    os.chdir('../../codigos')
    return('pipeta likeada successfulmente')


