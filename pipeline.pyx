#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as np
import subprocess
import shlex
from astroML.crossmatch import crossmatch
from astropy.io.fits import getheader
from astropy.table import Table

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------------------WARNINGS-----------------------------------#
#This program uses Source Extractor (from Emmanuel Bertin), this is software 
#from Astromatic.net, for further information visit
# https://www.astromatic.net/software/sextractor
# or send an e-mail to the author bertin@iap.fr
#----------------------------------------------------------------------------%
#this script uses path definitions were there should be the following objects:
        #Configuration files for SOURCE EXTRACTOR
        #The images to analize        
	#the future catalogs to be printed by this program    
#IT IS EXTREMELY IMPORTANT TO CHANGE THEM 
#-----------------------------------------------------------------------------#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#-----------------------------------------------------------------------------#
def count_lines2(path):
	lines= sum(1 for line in open(path))
	return(lines)
#-----------------------------------------------------------------------------#

#------------------------Diff_capsule_corp------------------------------------#
#-----------------------------------------------------------------------------#
from astropy.io import fits
from astropy.io import ascii
from astropy.time import Time

def Diff_capsule_corp(p1,gal, t, t_exp, i,zero,FWHM,res):
	path=os.path.abspath(p1)
	s=np.shape(gal)
	for l in range(0,s[0]):
		for j in range(0, s[1]):
			gal[l,j]=round(gal[l,j])
	file1=fits.PrimaryHDU(gal)
	hdulist=fits.HDUList([file1])
	hdr=hdulist[0].header
	time=Time(t, format='jd', scale='utc')
	dia = time.iso[0:10]
	hora = time.iso[11:24]
	jd= time.jd
	hdr.set('TIME-OBS', hora)
	hdr.set('DATE-OBS', dia)
	hdr.set('EXPTIME', t_exp)
	hdr.set('JD', jd)
	hdr.set('ZERO_P',zero)
	hdr.set('PX_SCALE',res)
	hdr.set('S_FWHM',FWHM)
	path_fits=os.path.join(path,('Diff_image'+str(i).zfill(5)+'.fits'))
	hdulist.writeto(path_fits)
#-----------------------------------------------------------------------------#
#-----------------------------------------------------------------------------#


#-----------------------Header function   changer-----------------------------#
#-----------------------------------------------------------------------------#
def changer(path):
	meses = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	image = fits.open(path, mode ='update')
	Hdr   = image[0].header
	__existence__ = True
	try:
		Hdr['JD']
	except KeyError as e:
		__existence__ = False
	if not __existence__:
		date  = Hdr['DATE-OBS']
		time  = Hdr['TIME']
		#los espacios estan en [0:7]
		#para date:::   [7:11] es el año, [12:15] es el mes, [15:] la fecha
		#para time:::   [7:] es la hora en formato ISO date_hms
		hour  = time[7:]
		year  = date[7:11]
		month = date[12:15]
		day   = date[16:]
		month = str(int(np.where(np.asarray(meses) == month)[0])+1).zfill(2)
		TIME = year+'-'+month+'-'+day+' '+hour
		t = Time(TIME, format = 'iso', scale = 'utc')
		#print t.jd
		Hdr.set('JD', t.jd)
		#image.flush()
		image.close()
#-----------------------------------------------------------------------------#
#-----------------------------------------------------------------------------#
def pipeline(path_test, DIA):
	origin_path = os.path.abspath('.')
	#getting the absolute path to the images directory
	p=os.path.abspath(path_test)
	#moving to it
	os.chdir(p)
	config = os.path.abspath('/opt/Bruno/Documentos/Codigos/image-simulation-and-analysis/default.sex')
	filtername = os.path.abspath('/opt/Bruno/Documentos/Codigos/image-simulation-and-analysis/default.conv')
	nnw_name = os.path.abspath('/opt/Bruno/Documentos/Codigos/image-simulation-and-analysis/default.nnw')
	pars_name = os.path.abspath('/opt/Bruno/Documentos/Codigos/image-simulation-and-analysis/default.param')
	#create a string naming the future image list
	l='listafits.dat'
	path1=os.path.abspath('/opt/Bruno/Imagenes/master/master_rotated.fits')
	#use an unix command to fill the list with our images names
	os.system('ls image*.fits > '+l)
	#getting length of the list
	cdef int number, i,j,k
	number=count_lines2(os.path.abspath(l))
	print(number)
#--------------------------------------------------------------------
	#open the list file
#--------------------------------------------------------------------
	listafits=open(os.path.join(p,l))
#--------------------------------------------------------------------
	#reading the sextractor catalog for master
#--------------------------------------------------------------------
	mastercat = os.path.abspath('/opt/Bruno/Imagenes/master/master-cat.cat')
	MASTER_CAT = ascii.read(mastercat, format='sextractor')
	MASTER_CAT['SOURCE_ID']=MASTER_CAT['NUMBER']
#-------#-------------------------------------------------------------------
	#starting the loop of the total of the images
#-------#-------------------------------------------------------------------
	headerline = ' NUMBER MAG_ISOCOR MAGERR_ISOCOR FLUX_APER FLUXERR_APER MAG_APER MAGERR_APER MAG_AUTO MAGERR_AUTO MAG_BEST MAGERR_BEST X_IMAGE Y_IMAGE A_IMAGE ERRA_IMAGE B_IMAGE ERRB_IMAGE ELLIPTICITY FLAGS CLASS_STAR JD_TIME SOURCE_ID'
	for i in MASTER_CAT['SOURCE_ID']:
		try:
			status = os.stat('/opt/Bruno/Imagenes/lightcurves/lc_'+str(i).zfill(6)+'.dat')
		except:
			f = open('/opt/Bruno/Imagenes/lightcurves/lc_'+str(i).zfill(6)+'.dat', 'a+')
			f.write(headerline)
			f.write('\n')
			f.close()
	masterXY = np.empty((len(MASTER_CAT), 2), dtype=np.float64)
	masterXY[:,0] = MASTER_CAT['X_IMAGE']
	masterXY[:,1] = MASTER_CAT['Y_IMAGE']
	for i in range(number):
#--------------------------------------------------------------------
		#reading fits name
	#----------------------------------------------------------------
		name=listafits.readline()
		#print(name) #  --- for debugging
		path_fits=os.path.abspath(name)
	#----------------------------------------------------------------
		#callling Header time changer for JD date use
		#changer(path_fits)  #deprecated
	#----------------------------------------------------------------
		#catalog creation and path location
		f=open('cat_'+str(name[0:-6])+'.cat', 'a+')
		f.close()
		catalog=os.path.join(p,'cat_'+str(name[0:-6])+'.cat')
		#print(path_fits)  #  ---- for debugging
	#----------------------------------------------------------------
		#getting image's header, for reading the date, and further details (in case of need)
	#----------------------------------------------------------------
		Hdr=getheader(path_fits)
		#for the moment just using the date
	#----------------------------------------------------------------
		#t=Hdr['JD']
	#----------------------------------------------------------------
		magzero = Hdr['MAGZERO']
		t = Hdr['DATE-OBS']
		t = Time(t, format='isot', scale='utc')
		#fw=round(float(Hdr['S_FWHM']),2)
		#px_s=round(float(Hdr['PX_SCALE']),3)
	#----------------------------------------------------------------
	#----------------------------------------------------------------
		#execute sextractor
	#----------------------------------------------------------------
		    #building commands
	#----------------------------------------------------------------
		command='sex '+path_fits+' -c '+config+' -CATALOG_NAME '+catalog+' -MAG_ZEROPOINT '+str(magzero)+' -FILTER_NAME '+filtername+' -STARNNW_NAME '+nnw_name+' -PARAMETERS_NAME '+pars_name# -PIXEL_SCALE '+str(px_s)+' -SEEING_FWHM '+str(fw) 
		arg=shlex.split(command)
	#----------------------------------------------------------------
		    #calling them
	#----------------------------------------------------------------
	#----------------------------------------------------------------
		subprocess.call(arg)
		print 'catalogo generado'
	#----------------------------------------------------------------
		#opening catalogs from SE using ascii.tables
	#----------------------------------------------------------------
		cat = ascii.read(catalog, format='sextractor')
		print 'catalogo leido'
	#----------------------------------------------------------------
		#filling a new column with the JD time
	#----------------------------------------------------------------
		col_tiempo = [t.jd for k in xrange(len(cat))]
		print 'tiempos creados'
		cat['JD_TIME'] = col_tiempo
		print 'columna de tiempo adherida'
	#----------------------------------------------------------------
		#creating lists of XY positions for matching
	#----------------------------------------------------------------
		imageXY = np.empty((len(cat), 2), dtype=np.float64)
		imageXY[:,0] = cat['X_IMAGE']
		imageXY[:,1] = cat['Y_IMAGE']
		print 'estructura de matching creada'
	#----------------------------------------------------------------
		#applying the crossmatching
	#----------------------------------------------------------------
		dist, ind = crossmatch( imageXY, masterXY, max_distance=2.5)
		print 'matching finalizado'
	#----------------------------------------------------------------
		#dropping the failed matches
	#----------------------------------------------------------------
		match = ~np.isinf(dist)
	#----------------------------------------------------------------
		#giving the IDs for the new sources matched			trabajar con los failed
	#----------------------------------------------------------------
		IDs = np.arange(len(cat))
		IDs[match] = MASTER_CAT['SOURCE_ID'][ind[match]]
		IDs[~match]= -13133
		cat['SOURCE_ID'] = IDs
		print 'IDs adheridos, escribiendo curvas de luz'
	#----------------------------------------------------------------
		#starting loop over the catalog lines
	#----------------------------------------------------------------
		cat_arr = np.asarray(cat)
		for j in range(len(cat)):
			if not cat_arr[j]['SOURCE_ID'] == -13133:
				line = ''
				for k in range(len(cat_arr[j])):
					line += ' '+str(cat_arr[j][k]) 
				with open('/opt/Bruno/Imagenes/lightcurves/lc_'+str(cat_arr[j]['SOURCE_ID']).zfill(6)+'.dat', 'a') as lc:
				#foo = cat[j:j+1]
				#lc.write(str(foo.pformat(show_name=False, show_unit=False, max_width = 3000, max_lines=2)[0]))
					lc.write(line)
					lc.write('\n')
		print 'Catalogos almacenados, siguiente imagen'
	print 'Fin de la pipeline de fotometria apertura'
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#-------rting the pipeline using Differential Image Photometry (DIA)
#--------------------------------------------------------------------
#--------------------------------------------------------------------
	if DIA:
		f =open('test_d.cat','a+')
		f.close()
		catalog=os.path.join(p,'test_d.cat')
		listafits=open(os.path.join(p,l))
		#creating catalog names for SE
		config=os.path.join('../../codigos','default.sex.differential')
		Sci_M = fits.open(path1)[0].data
		base_data=np.zeros(shape=(1,17))
		listafits.readline()
		for i in range(number-1):  
			path_fits=listafits.readline()
		#print path_fits   # ---- for debugging
			path_diff_fits = os.path.join(p,('Diff_image'+str(i+2).zfill(5)+'.fits'))
			if not os.path.exists(path_diff_fits):
			#using this to avoid crush if the file already exists
				if not path_fits == 'image00001.fits':
					#avoiding to use the first image, just the next ones
					#reading important information from the original fits header
					hdulist   =  fits.open(path_fits)
					scidata_U =  hdulist[0].data
					diff_data =  scidata_U - Sci_M
					t         =  hdulist[0].header['JD']
					t_exp     =  hdulist[0].header['EXPTIME']
					zero      =  hdulist[0].header['ZERO_P']
					FWHM      =  hdulist[0].header['S_FWHM']
					res       =  hdulist[0].header['PX_SCALE']
					#encapsuling the difference of images in a new fits
					Diff_capsule_corp(p,diff_data,t,t_exp,i+2,zero,FWHM,res)
			Hdr=fits.getheader(path_fits)
			tiempo=Hdr['JD']
			#execute sextractor
			    #building commands
			command='sex '+path_diff_fits+' -c '+config+' -CATALOG_NAME '+catalog#+' -PIXEL_SCALE '+str(px_s)+' SEEING_FWHM '+str(fw) 
			arg=shlex.split(command)
			    #calling them
			subprocess.call(arg)
			cat=open(catalog, 'r')
			#starting loop over the catalog lines
			for line in cat:
				if line:
					info = line.split()
					#print info  # ---- for debugging
					info.append(tiempo)
					base_data = np.append(base_data, [info], axis=0)
			#print base_data  # ---- for debugging
			cat.close()
		#print base_data   # ---- for debugging
		np.savetxt(fname='Diff.dat', X=base_data.astype(np.float))
		rscript=shlex.split('Rscript ../../codigos/engine.R')
		subprocess.call(rscript)
	os.chdir(origin_path)
	return('pipeta likeada successfulmente')


