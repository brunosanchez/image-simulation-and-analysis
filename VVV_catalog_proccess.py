#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as np
import subprocess
import shlex
from astroML.crossmatch import crossmatch
from astropy.table import Table
from astropy.io import ascii

#-----------------------------------------------------------------------------#
#------------Matching function------------------------------------------------#
#-----------------------------------------------------------------------------#
def matching(master, cat, angular=False, radius=2.5):
	from astroML import crossmatch as cx
	if angular:
		masterRaDec = np.empty((len(master), 2), dtype=np.float64)
		masterRaDec[:,0] = master['ALPHA_J2000']
		masterRaDec[:,1] = master['DEC_J2000']
		imRaDec = np.empty((len(cat), 2), dtype=np.float64)
		imRaDec[:,0] = cat['ALPHA_J2000']
		imRaDec[:,1] = cat['DEC_J2000']
		five_arcsecs= 5./3600.
		dist, ind = cx.crossmatch_angular(masterRaDec, imRaDec, max_distance=five_arcsec/2.)
		dist_, ind_ = cx.crossmatch_angular( imRaDec, masterRaDec, max_distance=five_arcsec/2.)
	else:
		masterXY = np.empty((len(master), 2), dtype=np.float64)
		masterXY[:,0] = master['X_IMAGE']
		masterXY[:,1] = master['Y_IMAGE']
		imXY = np.empty((len(cat), 2), dtype=np.float64)
		imXY[:,0] = cat['X_IMAGE']
		imXY[:,1] = cat['Y_IMAGE']
		dist, ind = cx.crossmatch(masterXY, imXY, max_distance=radius)
		#print dist, ind
		dist_, ind_ = cx.crossmatch( imXY, masterXY, max_distance=radius)
		#print dist_, ind_
	match = ~np.isinf(dist)
	#print match
	match_= ~np.isinf(dist_)
	#print match_
	IDs = np.zeros(len(cat), dtype=np.int)
	for i in range(len(cat['NUMBER'])):
		if match[i]: #fuente NUMERO i+1
			i_imag_pointer = master['SOURCE_ID'][ind[i]] # a donde va la fuente cat[i]
			if match_[i_imag_pointer - 1]: # si no es infinito fuente
				handshake = i+1 == cat['NUMBER'][ind_[i_imag_pointer - 1]]
				if handshake:
					IDs[i] = i_imag_pointer
		else:
			IDs[i] = -13133
	return(IDs)
#-----------------------------------------------------------------------------#
#-----------------------------------------------------------------------------#


#read master catalog list
master_path = '/opt/Bruno/VVV_pawprint/master_list2.cat'

master_path = os.path.abspath(master_path)

master_names=['ra_k','dec_k','mag_k','mag_err_k']

master_source = np.loadtxt(master_path,dtype={'names': ('ra_k','dec_k','mag_k','mag_err_k'), 'formats': ('f4', 'f4', 'f4', 'f4')})
#master_source_l = ascii.read(master_path, data_start=3, names = master_names)



